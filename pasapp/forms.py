from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import Group, User


class NewUserForm(UserCreationForm):
    email = forms.EmailField(label='email', max_length=128, required=True)
    isProfessor = forms.BooleanField(
        label='Are you a professor?', required=False)

    class Meta:
        model = User
        fields = ("username", "email", "first_name", "last_name",
                  "password1", "password2", "isProfessor")

    def save(self, commit=True):
        user = super(NewUserForm, self).save(commit=False)
        user.email = self.cleaned_data['email']

        if commit:
            user.save()

        user_group = 'professor' if self.cleaned_data['isProfessor'] else 'student'
        user.groups.add(Group.objects.get(name=user_group))

        return user


class NewProjectForm(forms.Form):
    title = forms.CharField(label="Title", max_length=200, widget=forms.Textarea)
    description = forms.CharField(
        label="Description", max_length=2048, widget=forms.Textarea)
    capacity = forms.IntegerField(label="Expected capacity of students/groups that can work on this project:", required=False)
    status = forms.CharField(
        label="Status", max_length=128, initial="Open", widget=forms.HiddenInput)
    hidden = forms.BooleanField(
        widget=forms.CheckboxInput(attrs={'class': 'hidden'}), # hidden checkbox cannot use hiddeninput
        required=False, label="")
    tags = forms.CharField(widget=forms.HiddenInput, required=False)


class ApplicationForm(forms.Form):
    message = forms.CharField(label="Message", max_length=2048, widget=forms.Textarea)
    preempt = forms.BooleanField(label="Automatically accept an offer", required=False)
    additional_students = forms.CharField(widget=forms.HiddenInput, required=False)

class CommentForm(forms.Form):
    application = forms.CharField(widget=forms.HiddenInput)
    message = forms.CharField(label="", max_length=2048, widget=forms.Textarea)