from django.contrib import admin

from .models import *

# Register your models here.
admin.site.register(Project)
admin.site.register(Application)
admin.site.register(Tag)
admin.site.register(ProjectTag)
admin.site.register(UserTag)
admin.site.register(TagCategory)
admin.site.register(Comment)
