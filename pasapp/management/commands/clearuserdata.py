from django.core.management.base import BaseCommand
from pasapp.models import *

class Command(BaseCommand):
    help = 'Clear all user-generated data.'

    def add_arguments(self, parser):
        parser.add_argument('--delete_users', action='store_true', help="Also delete all non-admin users.")
        parser.add_argument('--delete_tag_categories', action='store_true', help="Also delete all Tag Categories.")

    def handle(self, *args, **options):
        wr = self.stdout.write
        wr_succ = lambda s: wr(self.style.SUCCESS(s))
        wr_not = lambda s: wr(self.style.NOTICE(s))
        wr_err = lambda s: wr(self.style.ERROR(s))

        wr_not("Deleting all applications")
        count, count_dict = Application.objects.all().delete()
        if count > 0:
            wr_succ(f"Deleted {count} applications.")
            wr_not(f'{count_dict}')
        else:
            wr_not("No applications to delete.")
        
        wr_not("Deleting all projects")
        count, count_dict = Project.objects.all().delete()
        if count > 0:
            wr_succ(f"Deleted {count} projects.")
            wr_not(f'{count_dict}')
        else:
            wr_not("No projects to delete.")

        if options['delete_users']:
            wr_not("Deleting all users")
            count, count_dict = User.objects.exclude(username='admin').delete()
            if count > 0:
                wr_succ(f"Deleted {count} users.")
                wr_not(f'{count_dict}') 
            else:
                wr_not("No users to delete.")

        wr_not("Deleting all tags")
        count, count_dict = Tag.objects.all().delete()
        if count > 0:
            wr_succ(f"Deleted {count} tags.")
            wr_not(f'{count_dict}')
        else:
            wr_not("No tags to delete.")

        if options['delete_tag_categories']:
            wr_not("Deleting all tag categories")
            count, count_dict = TagCategory.objects.all().delete()
            if count > 0:
                wr_succ(f"Deleted {count} tag categories.")
                wr_not(f'{count_dict}')
            else:
                wr_not("No tag categories to delete.")
        
        wr_not("Done.")
