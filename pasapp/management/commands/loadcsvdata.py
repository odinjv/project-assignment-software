from django.core.management.base import BaseCommand
from django.apps import apps
from ._readcsv import read_csv
import os

def dir_path(string):
    if os.path.isdir(string):
        return string
    else:
        raise NotADirectoryError(string)

class Command(BaseCommand):
    help = 'Loads a test dataset into the database from a folder.'

    def add_arguments(self, parser):
        parser.add_argument('folder_path', nargs=1, type=dir_path)

    def handle(self, *args, **options):
        directory = options['folder_path'][0]
        if directory:
            read_csv(os.path.normpath(directory))
