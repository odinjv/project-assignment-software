from django.core.management.base import BaseCommand, CommandError
from django.core.management import call_command
from pasapp.models import *
from pasapp.forms import NewUserForm

class Command(BaseCommand):
    help = 'Adds a basic set of users and user data to the database. Calls populatedefaults'

    def make_test_users(self):
        userdata = [
            {
                'username': 'alicea',
                'email': 'alicea@stud.ntnu.no',
                'first_name': 'Alice',
                'last_name': 'Alaine',
                'password1': 'password',
                'password2': 'password',
                'isProfessor': False
            },
            {
                'username': 'bobbo',
                'email': 'bobbo@stud.ntnu.no',
                'first_name': 'Bob',
                'last_name': 'Bottom',
                'password1': 'password',
                'password2': 'password',
                'isProfessor': False
            },
            {
                'username': 'celinec',
                'email': 'celinec@idi.ntnu.no',
                'first_name': 'Celine',
                'last_name': 'Cadillac',
                'password1': 'password',
                'password2': 'password',
                'isProfessor': True
            },
            {
                'username': 'damiand',
                'email': 'damiand@ntnu.no',
                'first_name': 'Damian',
                'last_name': 'Demerera',
                'password1': 'password',
                'password2': 'password',
                'isProfessor': True
            },
        ]

        # Make test users
        successful_user_count = 0
        for user in userdata:
            form = NewUserForm(user)
            if form.is_valid():
                form.save()
                successful_user_count += 1
            else:
                self.stdout.write(self.style.ERROR(f'User {user["first_name"]} could not be created.'))
        self.stdout.write(self.style.SUCCESS(f'Created {successful_user_count} users.'))

    def make_test_tags(self, tag_cats):
        prereq_cat = tag_cats.get(category='Prerequisite Class')
        spec_cat = tag_cats.get(category='Specialization')
        subject_cat = tag_cats.get(category='Subject')
        topic_cat = tag_cats.get(category='Topic of Interest')
        method_cat = tag_cats.get(category='Method')
        
        # (name, category)
        tagsdata = [
            ('Mathematics 4', prereq_cat),
            ('Exphil', prereq_cat),
            ('Artificial Intelligence Programming', prereq_cat),
            ('Computer Vision and Deep Learning', prereq_cat),
            ('Graphics and Visualization', prereq_cat),
            ('Physics', prereq_cat),
            ('Software Systems', spec_cat),
            ('Databases and Search', spec_cat),
            ('Artificial Intelligence', spec_cat),
            ('Algorithms and Computers', spec_cat),
            ('Integrated Systems', spec_cat),
            ('3D Rendering', subject_cat),
            ('AI', subject_cat),
            ('Games', subject_cat),
            ('Computer Vision', subject_cat),
            ('Environment', topic_cat),
            ('Education', topic_cat),
            ('Marine Life', topic_cat),
            ('User Testing', method_cat),
            ('Literature Review', method_cat),
            ('Machine Learning', method_cat),
            ('Programming', method_cat),
        ]
        
        # Make test tags
        tags = [Tag(name=name, category=category) for (name, category) in tagsdata]
        successful_tags = Tag.objects.bulk_create(tags, ignore_conflicts=True)
        if len(successful_tags) == 0:
            self.stdout.write(self.style.NOTICE('No tags created.'))
        else:
            output_string = f'Created {len(successful_tags)} tags: ' + ', '.join(tag.name for tag in successful_tags)
            self.stdout.write(self.style.SUCCESS(output_string))

    def make_test_projects(self):
        projectdata = [
            {
                'title': 'AI to Catalog Undersea Life',
                'professor': 'celinec',
                'description': 'Develop a computer vision system to identify undersea life from research vessel photos.',
                'status': 'Open',
                'hidden': False,
                'tags': ('Artificial Intelligence', 'Artificial Intelligence Programming',
                        'AI', 'Environment', 'Marine Life', 'Machine Learning',
                        'Computer Vision and Deep Learning', 'Computer Vision')
            },
            {
                'title': 'Real-time volumetric particle rendering',
                'professor': 'damiand',
                'description': 'Explore new techniques in real-time 3D rendering of particle volumes.',
                'status': 'Open',
                'hidden': False,
                'tags': ('3D Rendering', 'Algorithms and Computers',
                        'Literature Review', 'Programming', 'Mathematics 4',
                        'Graphics and Visualization', 'Physics')
            },
        ]

        projects = [Project(title=project['title'],
                            professor=User.objects.get(username=project['professor']),
                            description=project['description'],
                            status=project['status'],
                            hidden=project['hidden'])
                            for project in projectdata]
        successful_projects = Project.objects.bulk_create(projects, ignore_conflicts=True)
        if len(successful_projects) == 0:
            self.stdout.write(self.style.NOTICE('No projects created.'))
        else:
            output_string = f'Created {len(successful_projects)} projects: ' + ', '.join(project.title for project in successful_projects)
            self.stdout.write(self.style.SUCCESS(output_string))

        # Add tags for test projects
        project_tags = [ProjectTag(project=Project.objects.filter(title=project['title']).latest(), tag=Tag.objects.get(name=tag))
                        for project in projectdata
                        for tag in project['tags']]
        successful_project_tags = ProjectTag.objects.bulk_create(project_tags, ignore_conflicts=True)
        if len(successful_project_tags) == 0:
            self.stdout.write(self.style.NOTICE('No project tags created.'))
        else:
            output_string = f'Created {len(successful_project_tags)} project tags.'
            self.stdout.write(self.style.SUCCESS(output_string))
    
    def make_test_applications(self): # TODO: will default statuses kick in?
        applicationdata = [
            {
                'project': 'AI to Catalog Undersea Life',
                'student': 'alicea',
                'message': 'This project is really interesting to me! Please can I work on it :)',
                'priority': 1
            },
            {
                'project': 'AI to Catalog Undersea Life',
                'student': 'bobbo',
                'message': 'I was somewhat interested in this project, but am not experienced in AI. Is there a chance I could take the project with a focus on literature review?',
                'priority': None
            },
            {
                'project': 'Real-time volumetric particle rendering',
                'student': 'bobbo',
                'message': 'I have been researching this in my spare time already, so I would be a great fit for this project.',
                'priority': 1
            },
        ]

        applications = [Application(project=Project.objects.filter(title=app['project']).latest(), message=app['message'],
                                    student=User.objects.get(username=app['student']), priority=app['priority'])
                        for app in applicationdata]
        successful_applications = Application.objects.bulk_create(applications, ignore_conflicts=True)
        if len(successful_applications) == 0:
            self.stdout.write(self.style.NOTICE('No applications created.'))
        else:
            output_string = f'Created {len(successful_applications)} applications.'
            self.stdout.write(self.style.SUCCESS(output_string))

    def handle(self, *args, **options):
        tag_cats = TagCategory.objects.all()
        try:
            if tag_cats.count() == 0:
                call_command('populatedefaults')
                tag_cats = TagCategory.objects.all()
        except:
            raise CommandError("Tag categories missing and unable to be created. Command aborted.")

        self.make_test_users()
        self.make_test_tags(tag_cats)
        self.make_test_projects()
        self.make_test_applications()
        