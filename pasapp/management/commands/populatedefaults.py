from django.core.management.base import BaseCommand
from pasapp.models import TagCategory

class Command(BaseCommand):
    help = 'Adds default set of tag categories to the database.'

    # (category, )
    dataset = [
        ('Prerequisite Class', True), # academic. subject ID/name
        ('Specialization', True), # academic. e.g. "Software Development" or "Databases" 
        ('Subject', False), # personal. e.g. "3D Rendering" or "Games", CS subject matters
        ('Topic of Interest', False), # personal. e.g. "Healthcare Systems" or "Sustainability", non-CS subject matter
        ('Method', False) # personal. e.g. "User Testing", "Literature Review", "Circuit Design", or "Software Prototyping"
    ]
    default_categories = [TagCategory(category=category, academic=is_academic) for (category, is_academic) in dataset]

    def handle(self, *args, **options):
        succeeded = TagCategory.objects.bulk_create(self.default_categories)
        if len(succeeded) == 0:
            self.stdout.write(self.style.NOTICE('No categories created.'))
        else:
            output_string = f'Created {len(succeeded)} categories: ' + ', '.join(cat.category for cat in succeeded)
            self.stdout.write(self.style.SUCCESS(output_string))