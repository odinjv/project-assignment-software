from datetime import datetime, timedelta, timezone, tzinfo
import logging
from django.core.exceptions import *
import csv
import os
from distutils.util import strtobool
from contextlib import contextmanager

from django.db import IntegrityError
from pasapp.models import ProjectTag, TagCategory, Tag, UserTag, Project, Application, Comment
from pasapp.forms import NewUserForm
from django.contrib.auth.models import User

to_bool = lambda str: bool(strtobool(str))

short_map = {'w': 'weeks', 'd': 'days', 'h': 'hours', 'm': 'minutes'}
cest_tz = timezone(timedelta(hours=+5), 'CEST')

def read_csv(input_folder, username_getter=None):
    if username_getter == None:
        username_getter = lambda x: x

    tag_category_map = {}
    tag_map = {}
    project_map = {}
    application_map = {}
    comment_map = {}

    output_stats = {}

    def stats_add(key, value):
        if key in output_stats:
            output_stats[key] += value
        else:
            output_stats[key] = value
    
    def get_or_throw(map, key):
        if key in map:
            return map[key]
        raise ObjectDoesNotExist("{key} not found.")
    
    def get_params(keys, row, skipped=()):
        output = {}
        for key, col in keys:
            if key == 'ref_id': continue
            if key in skipped: continue
            if key.startswith('f_') or key.startswith('ff_'): continue # foreign keys, case-by-case handling
            value = row[col]
            if value == '': continue # uses model default, use ' ' if a blank string is needed
            if key == 'username':
                value = username_getter(value)
            elif key.startswith('b_'): # boolean fields
                key = key[2:]
                value = to_bool(value)
            elif key.startswith('i_'): # integer fields
                key = key[2:]
                value = int(value)
            elif key.startswith('d_'): # datetime fields
                key = key[2:]
                if value.startswith('n-'): # shorthand for datetimes in relative past to when the data was populated
                    # (n)ow minus: (w)eeks, (d)ays, (h)ours, (m)inutes: n-1w2h25m
                    args = {}
                    n = ''
                    for c in value[2:]:
                        if c in '0123456789':
                            n+=c
                        elif c in short_map:
                            args[short_map[c]] = int(n)
                            n = ''
                    value = datetime.now(tz=cest_tz) - timedelta(**args)
                else:
                    # 1998.11.21 14:46:55 -> 1998, 11, 21, 14, 46, 55
                    value = datetime(*[int(n) for n in value.replace('.', ':').replace(' ', ':').split(':')], tzinfo=cest_tz)
            output[key] = value
        return output

    def read_tag_categories(file):
        # ref_id, category (name), (b_academic)
        col_map = {}
        created_cnt = 0
        failed_cnt = 0
        existing_cnt = 0
        error_cnt = 0
        def cnt_increment():
            nonlocal error_cnt
            error_cnt += 1
        with open(file, encoding="utf-8") as f:
            reader = csv.reader(f)
            error_logger = ErrorLogger('tagcategories', cnt_increment)

            for row_num, row in enumerate(reader):
                if row_num == 0:
                    for i, val in enumerate(row):
                        col_map[val] = i
                    if 'ref_id' not in col_map:
                        logging.error(f"'tagcategories.csv' is missing necessary column 'ref_id'")
                        return
                else:
                    ref_id = row[col_map['ref_id']]
                    params = get_params(col_map.items(), row)
                    with error_logger.handle(row_num):
                        retrieved, created = TagCategory.objects.get_or_create(**params)
                        if retrieved:
                            tag_category_map[ref_id] = retrieved.pk
                            if created:
                                created_cnt += 1
                            else:
                                existing_cnt +=1
                        else:
                            logging.warning(f"Could not create TagCategory {params['category']}, ref_id {ref_id}. Found at row {row_num} of 'tagcategories.csv'.")
                            failed_cnt += 1
        if created_cnt > 0: stats_add('Tag Categories created', created_cnt)
        if existing_cnt > 0: stats_add('Tag Categories already existing', existing_cnt)
        if failed_cnt > 0: stats_add('Tag Categories failed', failed_cnt+error_cnt)
        if error_cnt > 0: stats_add('Errors occured while creating Tag Categories', error_cnt)

    def read_tags(file):
        # (ref_id), name, f_category
        col_map = {}
        created_cnt = 0
        failed_cnt = 0
        existing_cnt = 0
        error_cnt = 0
        def cnt_increment():
            nonlocal error_cnt
            error_cnt += 1
        with open(file, encoding="utf-8") as f:
            reader = csv.reader(f)
            error_logger = ErrorLogger('tags', cnt_increment)

            for row_num, row in enumerate(reader):
                if row_num == 0:
                    for i, val in enumerate(row):
                        col_map[val] = i
                    for key in ('name', 'f_category'):
                        if key not in col_map:
                            logging.error(f"'tags.csv' is missing necessary column '{key}'")
                            return
                else:
                    ref_id = row[col_map['ref_id']] if 'ref_id' in col_map else False
                    name = row[col_map['name']]
                    with error_logger.handle(row_num):
                        category_id = get_or_throw(tag_category_map, row[col_map['f_category']])
                        retrieved, created = Tag.objects.get_or_create(name=name, category_id=category_id)
                        if retrieved:
                            if ref_id:
                                tag_map[ref_id] = retrieved.pk
                            if created:
                                created_cnt += 1
                            else:
                                existing_cnt +=1
                        else:
                            logging.warning(f"Could not create Tag {name}, ref_id {ref_id}. Found at row {row_num} of 'tags.csv'.")
                            failed_cnt += 1
        if created_cnt > 0: stats_add('Tags created', created_cnt)
        if existing_cnt > 0: stats_add('Tags already existing', existing_cnt)
        if failed_cnt > 0: stats_add('Tags failed', failed_cnt+error_cnt)
        if error_cnt > 0: stats_add('Errors occured while creating tags', error_cnt)
    
    def read_users(file):
        # username, email, (first_name, last_name, b_isProfessor, [ff_tags]), password -> 'password'
        col_map = {}

        created_cnt = 0
        failed_cnt = 0
        existing_cnt = 0

        created_tag_cnt = 0
        failed_tag_cnt = 0
        existing_tag_cnt = 0
        error_tag_cnt = 0
        def tag_cnt_increment():
            nonlocal error_tag_cnt
            error_tag_cnt += 1

        tag_error_logger = ErrorLogger('(tags) users', tag_cnt_increment)
        with open(file, encoding="utf-8") as f:
            reader = csv.reader(f)
            for row_num, row in enumerate(reader):
                if row_num == 0:
                    for i, val in enumerate(row):
                        col_map[val] = i
                    for key in ('username', 'email'):
                        if key not in col_map:
                            logging.error(f"'users.csv' is missing necessary column '{key}'")
                            return
                else:
                    form_args = get_params(col_map.items(), row, ('password',))
                    form_args['password1'] = form_args['password2'] = 'password'
                    user = None
                    try:
                        user = User.objects.get(username=form_args['username'])
                        existing_cnt += 1
                    except:
                        form = NewUserForm(form_args)
                        if form.is_valid():
                            form.save()
                            user = User.objects.get(username=form_args['username'])
                            created_cnt += 1
                        else:
                            logging.warning(f"Could not create User {form_args['username']}, found at row {row_num} of 'users.csv':")
                            logging.warning(form.errors)
                            failed_cnt += 1
                    if 'ff_tags' in col_map and user:
                        tags_ref_list = row[col_map['ff_tags']].split(';')
                        for tag_ref in tags_ref_list:
                            with tag_error_logger.handle(row_num, tag_ref):
                                tag_id = get_or_throw(tag_map, tag_ref)
                                user_tag, created_tag = UserTag.objects.get_or_create(user=user, tag_id=tag_id)
                                if not user_tag:
                                    failed_tag_cnt += 1
                                    continue
                                if created_tag:
                                    created_tag_cnt += 1
                                else:
                                    existing_tag_cnt += 1
        if created_cnt > 0: stats_add('Users created', created_cnt)
        if existing_cnt > 0: stats_add('Users already existing', existing_cnt)
        if failed_cnt > 0: stats_add('Users failed', failed_cnt)
        if created_tag_cnt > 0: stats_add('UserTags created', created_tag_cnt)
        if existing_tag_cnt > 0: stats_add('UserTags already existing', existing_tag_cnt)
        if failed_tag_cnt > 0: stats_add('UserTags failed', failed_tag_cnt)
        if error_tag_cnt > 0: stats_add('Errors occured while creating UserTags', error_tag_cnt)

    def read_projects(file):
        # (ref_id), title, f_professor, ff_tags, (capacity, description, status, b_hidden, d_date_created, d_last_updated)
        col_map = {}
        created_cnt = 0
        failed_cnt = 0
        existing_cnt = 0
        error_cnt = 0
        created_tag_cnt = 0
        existing_tag_cnt = 0
        failed_tag_cnt = 0
        error_tag_cnt = 0
        def cnt_increment():
            nonlocal error_cnt
            error_cnt += 1
        def tag_cnt_increment():
            nonlocal error_tag_cnt
            error_tag_cnt += 1
        with open(file, encoding="utf-8") as f:
            reader = csv.reader(f)
            error_logger = ErrorLogger('projects', cnt_increment)
            tag_error_logger = ErrorLogger('(tags) projects', tag_cnt_increment)

            for row_num, row in enumerate(reader):
                if row_num == 0:
                    for i, val in enumerate(row):
                        col_map[val] = i
                    for key in ('f_professor', 'title', 'status'):
                        if key not in col_map:
                            logging.error(f"'projects.csv' is missing necessary column '{key}'")
                            return
                else:
                    with error_logger.handle(row_num):
                        ref_id = row[col_map['ref_id']] if 'ref_id' in col_map else None
                        professor = User.objects.get(username=username_getter(row[col_map['f_professor']]))
                        params = get_params(col_map.items(), row)
                        copies = Project.objects.filter(title=row[col_map['title']], professor=professor, status=row[col_map['status']])
                        if len(copies) > 0:
                            retrieved = copies[0]
                            created = False
                        else:
                            retrieved, created = Project.objects.get_or_create(professor=professor, **params)
                            # will never get, only create, due to no unique fields
                        if retrieved:
                            if ref_id:
                                project_map[ref_id] = retrieved.pk
                            if created:
                                created_cnt += 1
                            else:
                                existing_cnt += 1
                            if 'ff_tags' in col_map:
                                tags_ref_list = row[col_map['ff_tags']].split(';')
                                for tag_ref in tags_ref_list:
                                    with tag_error_logger.handle(row_num, tag_ref):
                                        tag_id = get_or_throw(tag_map, tag_ref)
                                        project_tag, created_tag = ProjectTag.objects.get_or_create(project=retrieved, tag_id=tag_id)
                                        if not project_tag:
                                            failed_tag_cnt += 1
                                            continue
                                        if created_tag:
                                            created_tag_cnt += 1
                                        else:
                                            existing_tag_cnt += 1
                        else:
                            logging.warning(f"Could not create Project with ref_id {ref_id}. Found at row {row_num} of projects.csv.")
                            failed_cnt += 1
        if created_cnt > 0: stats_add('Projects created', created_cnt)
        if existing_cnt > 0: stats_add('Projects already existing', existing_cnt)
        if failed_cnt > 0: stats_add('Projects failed', failed_cnt+error_cnt)
        if error_cnt > 0: stats_add('Errors occured while creating Projects', error_cnt)
        if created_tag_cnt > 0: stats_add('ProjectTags created', created_tag_cnt)
        if existing_tag_cnt > 0: stats_add('ProjectTags already existing', existing_tag_cnt)
        if failed_tag_cnt > 0: stats_add('ProjectTags failed', failed_tag_cnt)
        if error_tag_cnt > 0: stats_add('Errors occured while creating ProjectTags', error_tag_cnt)

    def read_applications(file):
        # (ref_id), f_project, f_student, (ff_members, i_priority, message, professor_status, student_status, d_date_created, d_last_updated)
        col_map = {}
        created_cnt = 0
        failed_cnt = 0
        existing_cnt = 0
        error_cnt = 0
        member_error_cnt = 0
        def cnt_increment():
            nonlocal error_cnt
            error_cnt += 1
        with open(file, encoding="utf-8") as f:
            reader = csv.reader(f)
            error_logger = ErrorLogger('applications', cnt_increment)

            for row_num, row in enumerate(reader):
                if row_num == 0:
                    for i, val in enumerate(row):
                        col_map[val] = i
                    for key in ('f_project', 'f_student'):
                        if key not in col_map:
                            logging.error(f"'applications.csv' is missing necessary column '{key}'")
                            return
                else:
                    with error_logger.handle(row_num):
                        ref_id = row[col_map['ref_id']] if 'ref_id' in col_map else None
                        project_id = get_or_throw(project_map, row[col_map['f_project']]) #Project.objects.get(id=row[col_map['f_project']])
                        username = username_getter(row[col_map['f_student']])
                        student = User.objects.get(username=username)
                        group_members = []
                        if 'ff_members' in col_map and row[col_map['ff_members']]:
                            group_members = row[col_map['ff_members']].split(';')
                        params = get_params(col_map.items(), row)
                        try:
                            retrieved, created = Application.objects.get_or_create(project_id=project_id, student=student, **params)
                            if retrieved:
                                if ref_id:
                                    application_map[ref_id] = retrieved.pk
                                if created:
                                    created_cnt += 1
                                else:
                                    existing_cnt +=1
                                for member in group_members:
                                    if not member:
                                        continue #empty string
                                    try:
                                        user = User.objects.get(username=username_getter(member))
                                        retrieved.group_members.add(user)
                                    except Exception as e:
                                        member_error_cnt += 1
                            else:
                                logging.warning(f"Could not create Application by {username} on project with refID '{row[col_map['f_project']]}'. Found at row {row_num} of applications.csv.")
                                failed_cnt += 1
                        except IntegrityError as e:
                            retrieved = Application.objects.get(project_id=project_id, student=student)
                            if ref_id:
                                application_map[ref_id] = retrieved.pk
                            existing_cnt +=1
                        except Exception as e:
                            raise e
        if created_cnt > 0: stats_add('Applications created', created_cnt)
        if existing_cnt > 0: stats_add('Applications already existing', existing_cnt)
        if failed_cnt > 0: stats_add('Applications failed', failed_cnt+error_cnt)
        if error_cnt > 0: stats_add('Errors occured while creating Applications', error_cnt)
        if member_error_cnt > 0: stats_add('Group Members not found', member_error_cnt)

    def read_comments(file):
        # (ref_id), f_application, f_sender, message, d_date_created
        col_map = {}
        created_cnt = 0
        failed_cnt = 0
        existing_cnt = 0
        error_cnt = 0
        def cnt_increment():
            nonlocal error_cnt
            error_cnt += 1
        with open(file, encoding="utf-8") as f:
            reader = csv.reader(f)
            error_logger = ErrorLogger('comments', cnt_increment)

            for row_num, row in enumerate(reader):
                if row_num == 0:
                    for i, val in enumerate(row):
                        col_map[val] = i
                    for key in ('f_application', 'f_sender', 'message'):
                        if key not in col_map:
                            logging.error(f"'comments.csv' is missing necessary column '{key}'")
                            return
                else:
                    with error_logger.handle(row_num):
                        ref_id = row[col_map['ref_id']] if 'ref_id' in col_map else None
                        application_id = get_or_throw(application_map, row[col_map['f_application']])
                        username = username_getter(row[col_map['f_sender']])
                        sender = User.objects.get(username=username)
                        params = get_params(col_map.items(), row)
                        existing = Comment.objects.filter(application_id=application_id, sender=sender, message=params['message']) # ignores duplicates at different datetimes
                        if existing:
                            retrieved = existing.last()
                            created = False
                        else:
                            retrieved, created = Comment.objects.get_or_create(application_id=application_id, sender=sender, **params)
                        if retrieved:
                            if ref_id:
                                comment_map[ref_id] = retrieved.pk
                            if created:
                                created_cnt += 1
                            else:
                                existing_cnt +=1
                        else:
                            logging.warning(f"Could not create Comment by {username} on application with refID '{row[col_map['f_application']]}'. Found at row {row_num} of applications.csv.")
                            failed_cnt += 1
        if created_cnt > 0: stats_add('Comments created', created_cnt)
        if existing_cnt > 0: stats_add('Comments already existing', existing_cnt)
        if failed_cnt > 0: stats_add('Comments failed', failed_cnt+error_cnt)
        if error_cnt > 0: stats_add('Errors occured while creating Comments', error_cnt)

    deferred_functions = {'tagcategories': read_tag_categories,
                          'tags': read_tags,
                          'users': read_users,
                          'projects': read_projects,
                          'applications': read_applications,
                          'comments': read_comments}
    
    files = [os.path.join(input_folder, file) for file in os.listdir(input_folder)]

    validfiles = [file for file in files if os.path.isfile(file) and file.endswith(".csv")]
    for modeltype in ['tagcategories', 'tags', 'users', 'projects', 'applications', 'comments']:
        modelfile = None
        for file in validfiles:
            if file.endswith(modeltype + ".csv"):
                modelfile = file
                break
        
        if modelfile is None:
            logging.warning(f'No file found for {modeltype}.')
            continue
        
        deferred_functions[modeltype](modelfile)
    
    for title, value in output_stats.items():
        print(f'{title}: {value}.')

    return

class ErrorLogger():
    def __init__(self, filename, counter_func):
        self.counter_func = counter_func
        self.filename = f"'{filename}.csv'"

    @contextmanager
    def handle(self, row_num, ref_id=None):
        try:
            row_num += 1
            error_raised = True
            if ref_id is None:
                position = f"model at row {row_num} in {self.filename}"
            else:
                position = f"refID '{ref_id}' in a field of the model at row {row_num} in {self.filename}"
            yield
        except FieldDoesNotExist:
            logging.error(f"One or more of the fields specified in the header of {self.filename} were not defined in the model.")
        except FieldError:
            logging.error(f"One or more of the fields specified in the header of {self.filename} were not defined in the model.")
        except ValidationError:
            logging.error(f"The {position} failed to validate. This is likely a missing foreign key issue.")
        except ObjectDoesNotExist as e:
            if ref_id is None:
                logging.error(f"A foreign key or refID at row {row_num} in {self.filename} was not found.")
                logging.error(e)
            else:
                logging.error(f"The refID '{ref_id}' at row {row_num} in {self.filename} was not found.")
        except Exception as e:
            logging.error(f"The {position} could not be imported.")
            logging.error(e)
        else:
            error_raised = False
        finally:
            if error_raised:
                self.counter_func()
