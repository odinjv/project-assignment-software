from django.db.models.query import QuerySet

from pasapp.models import Project, ProjectTag

OPEN_STATUS = "Open"
DRAFT_STATUS = "Draft"
CLOSED_STATUS = "Closed"
DEFAULT_STATUSES = (OPEN_STATUS, DRAFT_STATUS, CLOSED_STATUS)

def get_open_projects(inputSet: QuerySet = None) -> QuerySet:
    # return Project.objects.filter(status=OPEN_STATUS)
    if not inputSet:
        inputSet = Project.objects
    return inputSet.exclude(status=CLOSED_STATUS).exclude(status=DRAFT_STATUS).exclude(hidden=True) # Statuses are open by default

def get_open_tags(inputSet: QuerySet = None) -> QuerySet:
    if not inputSet:
        inputSet = ProjectTag.objects
    open_project_ids = get_open_projects().values_list('id', flat=True)
    return inputSet.filter(project__in=open_project_ids) # for performance, consider list(open_project_ids)