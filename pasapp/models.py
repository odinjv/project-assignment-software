from django.db import models
from django.db.models import Q
from django.contrib.auth.models import User
from django.utils.timezone import now

class Project(models.Model):
    title = models.CharField(max_length=200)
    professor = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    description = models.CharField(max_length=2048)
    group_size = models.CharField(max_length=128, blank=True, null=True)  # TODO: Not implemented
    status = models.CharField(max_length=128, default="Draft")
    hidden = models.BooleanField(default=True) # only in effect if status is custom
    capacity = models.IntegerField(default=None, blank=True, null=True)
    date_created = models.DateTimeField(
        default=now, name='date_created')
    last_updated = models.DateTimeField(
        auto_now=True, name='last_updated')
    # Tags - see ProjectTag below

    class Meta:
        get_latest_by = "date_created"
        ordering = ["title"]
        indexes = [
            models.Index(fields=['professor'], name='project_by_professor_index'),
            models.Index(fields=['title'], name='project_by_title_index')
        ]

    def __str__(self):
        return f'{self.title}, by {self.professor}. Status: {self.status}'


class Application(models.Model):

    class ProfessorStatus(models.TextChoices):
        NONE = 'NONE'
        OFFERED = 'OFFR'
        DECLINED = 'DECL'
        RETRACTED = 'RETR'

    class StudentStatus(models.TextChoices):
        APPLIED = 'APPL'
        PREEMPTED = 'PRE', 'Preemptively Accepted'
        ACCEPTED = 'ACC'
        DECLINED = 'DECL'
        RETRACTED = 'RETR'

    def student_status_label(self):
        return Application.StudentStatus(self.student_status).label

    def professor_status_label(self):
        return Application.ProfessorStatus(self.professor_status).label

    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=True)
    student = models.ForeignKey(User, on_delete=models.CASCADE)
    group_members = models.ManyToManyField(User, related_name="member_projects", limit_choices_to=Q(groups__name = 'student'))
    message = models.CharField(max_length=2048)
    date_created = models.DateTimeField(default=now, name='date_created')
    last_updated = models.DateTimeField(
        auto_now=True, name='last_updated')
    priority = models.IntegerField(default=None, blank=True, null=True)
    professor_status = models.CharField(max_length=4, choices=ProfessorStatus.choices, default=ProfessorStatus.NONE, null=False)  
    student_status = models.CharField(max_length=4, choices=StudentStatus.choices, default=StudentStatus.APPLIED, null=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=('project', 'student'), name="unique_applications_per_project")
        ]
        get_latest_by = "date_created"
        ordering = [models.F("priority").asc(nulls_last=True), "-last_updated"]
        indexes = [
            models.Index(fields=['project', 'student'], name="app_by_project_index"),
            models.Index(fields=['student'], name='app_by_student_index'),
        ]

    def __str__(self):
        return f'{self.student} (+{self.group_members.count()}) on {self.project.title}. Status: {self.student_status_label()}, {self.professor_status_label()}'


class Comment(models.Model):
    application = models.ForeignKey(Application, on_delete=models.CASCADE)
    sender = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    date_created = models.DateTimeField(default=now, name='date_created')
    message = models.CharField(max_length=2048)

    class Meta:
        get_latest_by = "date_created"
        ordering = ["date_created"]
        indexes = [
            models.Index(fields=['application'], name="comment_by_application_index"),
        ]
    
    def __str__(self):
        return f'{self.sender} on {self.application.student}\'s application to {self.application.project.title}'

class TagCategory(models.Model):
    category = models.CharField(max_length=128, unique=True)
    academic = models.BooleanField(default=False)

    class Meta:
        verbose_name_plural = "tag categories"
        ordering = ['category']
        indexes = [
            models.Index(fields=['category'], name="tag_category_index"),
        ]

    def __str__(self):
        return self.category


class Tag(models.Model):
    name = models.CharField(max_length=256, unique=True)
    category = models.ForeignKey(
        TagCategory, on_delete=models.SET_NULL, blank=True, null=True, default=None)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=('name', 'category'), name="unique_tags_per_category")
        ]
        ordering = ["category", "name"]
        indexes = [
            models.Index(fields=['category'], name="tag_by_category_index"),
        ]

    def __str__(self):
        return self.name


class ProjectTag(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=('project', 'tag'), name="unique_tags_per_project")
        ]
        ordering = ["project", "tag"]
        indexes = [
            models.Index(fields=['project'], name="tag_by_project_index"),
        ]

    def __str__(self):
        return f'{self.project.title}: {self.tag.name}'


class UserTag(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    tag = models.ForeignKey(Tag, on_delete=models.CASCADE)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=('user', 'tag'), name="unique_tags_per_user")
        ]
        ordering = ['user', 'tag']
        indexes = [
            models.Index(fields=['user'], name='user_tag_index'),
        ]

    def __str__(self):
        return f'{self.user.first_name} {self.user.last_name}: {self.tag.name}'
