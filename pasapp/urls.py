from django.views.generic import RedirectView
from django.conf.urls import url
from django.urls import path

from . import views

urlpatterns = [
     path('', views.index, name='index'),
     path('login/', views.login_view, name='login'),
     path('logout/', views.logout_view, name='logout'),
     path('register/', views.register, name='register'),
     path('project/create/', views.create_project, name='create_project'),
     path('project/<int:project_id>/',
          views.project_details, name='project_details'),
     path('project/<int:project_id>/apply/', views.apply, name='apply'),
     path('project/<int:project_id>/applications/',
          views.project_applications, name='project_applications'),
     path('project/<int:project_id>/edit/',
          views.edit_project, name='edit_project'),
     path('project/<int:project_id>/copy/',
          views.copy_project, name='copy_project'),
     path('project/update_status/', views.set_project_status, name='update_project_status'),
     path('project/delete/', views.delete_project, name='delete_project'),
     path('projects/filter/', views.index, name='filter_projects'),
     path('projects/mine/', views.my_projects, name='my_projects'),
     path('applications/', views.my_applications, name='my_applications'),
     path('applications/update_priorities/',
          views.update_priorities, name='update_priorities'),
     path('applications/update_student_status/',
          views.update_student_status, name='update_student_status'),
     path('applications/update_professor_status/',
          views.update_professor_status, name='update_professor_status'),
     path('applications/add_comment/', views.add_comment, name='add_comment'),
     path('user/<str:username>/', views.profile_page, name='profile_page'),
     path('api/get_student/<str:username>', views.get_student, name='get_student'),
     url(r'^favicon\.ico$', RedirectView.as_view(url='/static/pasapp/favicon.ico')),
]
