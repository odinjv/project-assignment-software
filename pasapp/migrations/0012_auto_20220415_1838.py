# Generated by Django 3.2.9 on 2022-04-15 16:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pasapp', '0011_alter_project_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='project',
            name='hidden',
            field=models.BooleanField(default=True),
        ),
        migrations.AlterField(
            model_name='project',
            name='status',
            field=models.CharField(default='Draft', max_length=128),
        ),
    ]
