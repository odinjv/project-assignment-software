# Generated by Django 3.2.9 on 2022-04-06 04:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('pasapp', '0008_populate_necessary'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='application',
            options={'get_latest_by': 'date_created', 'ordering': ['priority', 'last_updated']},
        ),
        migrations.AlterModelOptions(
            name='project',
            options={'get_latest_by': 'date_created', 'ordering': ['title']},
        ),
        migrations.AlterModelOptions(
            name='projecttag',
            options={'ordering': ['project', 'tag']},
        ),
        migrations.AlterModelOptions(
            name='studenttag',
            options={'ordering': ['student', 'tag']},
        ),
        migrations.AlterModelOptions(
            name='tag',
            options={'ordering': ['category', 'name']},
        ),
        migrations.AlterModelOptions(
            name='tagcategory',
            options={'verbose_name_plural': 'tag categories'},
        ),
        migrations.AlterField(
            model_name='tagcategory',
            name='category',
            field=models.CharField(max_length=128, unique=True),
        ),
        migrations.AddConstraint(
            model_name='application',
            constraint=models.UniqueConstraint(fields=('project', 'student'), name='unique_applications_per_project'),
        ),
        migrations.AddConstraint(
            model_name='projecttag',
            constraint=models.UniqueConstraint(fields=('project', 'tag'), name='unique_tags_per_project'),
        ),
        migrations.AddConstraint(
            model_name='studenttag',
            constraint=models.UniqueConstraint(fields=('student', 'tag'), name='unique_tags_per_student'),
        ),
        migrations.AddConstraint(
            model_name='tag',
            constraint=models.UniqueConstraint(fields=('name', 'category'), name='unique_tags_per_category'),
        ),
    ]
