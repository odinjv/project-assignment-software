from django import template
from pasapp.forms import CommentForm
from pasapp.utils import tagsByCategory
from pasapp.templatetags.custom_filters import get as get_or_blank
from django.utils.safestring import mark_safe

register = template.Library()

@register.inclusion_tag('pasapp/components/status_select.html')
def status_select():
    return {  }

@register.inclusion_tag('pasapp/components/tagbubble.html')
def tagbubble(tag, onclick=False):
    return { 'tag': tag , 'onclick': onclick}


@register.inclusion_tag('pasapp/components/tagbox_static.html')
def tagbox_static(tag_pairs):
    return { 'tagPairs': tag_pairs }

@register.inclusion_tag('pasapp/components/tagbox_categorized.html')
def tagbox_categorized(tags = [], onclick=False, id="tagBox"):
    tags_by_category = tagsByCategory(tags)
    return { 'tagsByCategory': tags_by_category, 'onclick': onclick, 'id': id }

@register.inclusion_tag('pasapp/components/tagfilters.html')
def tagfilters(all_tags, selected_tags):
    tags_by_category = tagsByCategory(all_tags)
    return { 'tagsByCategory': tags_by_category, 'selectedTags': selected_tags }

def checkbox_base(mark, checked):
    checked_class = 'checked' if checked else ''
    return f'<div class="custom-checkbox {mark} {checked_class}" onclick="filterCheckbox(this)"></div>'

@register.filter()
def ternary_checkbox(tags, tag):
    state = get_or_blank(tags, tag)
    return mark_safe(checkbox_base('check', state == 'I') + checkbox_base('cross', state == 'E'))

@register.inclusion_tag('pasapp/components/tags_dropdown.html')
def tags_dropdown(tags):
    tags_by_category = tagsByCategory(tags)
    return { 'tagsByCategory': tags_by_category }

@register.inclusion_tag('pasapp/components/application_student.html')
def application_student(application, user, comments=[], interactive=False, comment_box=False):
    if interactive:
        checkbox = application.student_status in ('APPL', 'PRE') and application.professor_status in ('NONE')
        responses = application.professor_status in ('OFFR') and application.student_status in ('APPL', 'PRE') # 'PRE' not necessary after automation
    else:
        checkbox = responses = False
    student_status = application.student_status
    professor_status = application.professor_status
    return { 'application': application, 'user': user,
        'studentStatus': student_status, 'professorStatus': professor_status, 
        'checkbox': checkbox, 'responses': responses,
        'comments': comments, 'commentBox': comment_box }

@register.inclusion_tag('pasapp/components/application_professor.html')
def application_professor(application, comments=[], buttons=False, comment_box=False):
    return { 'application': application, 'buttons': buttons, 'comments': comments, 'commentBox': comment_box}

@register.inclusion_tag('pasapp/components/application_list_professor_interactive.html')
def application_list_professor_interactive(applications, comments=[], comment_box=False):
    # applications_by_category = {}
    return { 'applications': applications, 'comments': comments, 'commentBox': comment_box}

@register.inclusion_tag('pasapp/components/comment_box.html')
def comment_box(application_id):
    form = CommentForm(initial={'application': application_id,})
    return { 'form': form }

@register.inclusion_tag('pasapp/components/application_comment_section.html')
def application_comment_section(application_id, comments = [], comment_box=False):
    return {'id': application_id, 'comments': comments, 'commentBox': comment_box}
