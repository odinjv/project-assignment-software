from django import template
from django.utils.safestring import mark_safe
from django.utils.html import escape

register = template.Library()


@register.filter(name='is_professor')
def is_professor(user):
    return user.groups.filter(name='professor').exists()

@register.filter(name='is_student')
def is_student(user):
    return user.groups.filter(name='student').exists()

@register.filter(name='academic')
def filter_academic(tags, academic=True):
    return [tag for tag in tags if tag.category.academic == academic]

@register.filter(name='student_status')
def student_status(application):
    return application.student_status_label()

@register.filter(name='professor_status')
def professor_status(application):
    return application.professor_status_label()

@register.filter(name='is_closed')
def is_closed(application):
    closed = application.student_status in ('RETR', 'DECL')
    closed = closed or application.professor_status == 'DECL'
    return closed

@register.filter(name="name_link")
def name_link(user):
    linked_name = f'<a href="/user/{escape(user.username)}" class="undecorated-link underline-hover">'
    linked_name += f'<b>{escape(user.first_name)} {escape(user.last_name)}</b></a>'
    return mark_safe(linked_name)

@register.filter(name="name_list")
def name_list(users, clickable=False):
    if clickable:
        out = ', '.join([name_link(user) for user in users])
        return mark_safe(out)
    else:
        return ', '.join([user.first_name + ' ' + user.last_name for user in users])

@register.filter(name='minify')
def minify(label):
    mapping = {
        'Preemptively Accepted': 'Auto-accept'
    }
    return label if label not in mapping else mapping[label]

@register.filter(name='for_project')
def for_project(project_tags, project):
    return [tag for tag in project_tags if tag.project.id == project.id] # TODO: check if we can guarantee project_tags is QuerySet

@register.filter(name='times')
def times(number):
    return range(1, number + 1)

@register.filter(name='for_application')
def for_application(comments, application):
    return comments.filter(application=application) if comments else [] # TODO: check if we can guarantee comments is QuerySet

@register.filter(name='get')
def get(map, key):
    return map[key] if key in map else ''

@register.filter(name='at')
def indexable_at(tuple, index):
    return tuple[index]