from django.contrib.auth.forms import AuthenticationForm
from pasapp.models import Application

def contextWithHeader(context, request):
    context['user'] = request.user
    context['loginForm'] = AuthenticationForm()
    return context

def tagsByCategory(tags):
    tags_by_category = {}
    for tag in tags:
        if tag.category in tags_by_category:
            tags_by_category[tag.category].append(tag)
        else:
            tags_by_category[tag.category] = [tag,]
    return tags_by_category

def get_next(request, fallback='/'):
    next = request.GET.get('next', fallback)
    if next is None or next == "":
        return fallback
    return next

def student_accepted(application):
    student = application.student
    student_accepted_helper(student)
    group_students = application.group_members.all()
    for groupmate in group_students:
        student_accepted_helper(groupmate)
    
def student_accepted_helper(student):
    other_applications = Application.objects.filter(student=student).exclude(student_status='ACC')
    for app in other_applications:
        app.student_status = 'RETR' if app.professor_status != 'OFFR' else 'DECL'
        app.priority = None
        app.save()
    other_member_applications = student.member_projects.exclude(student_status='ACC')
    for app in other_member_applications:
        app.group_members.remove(student)

def project_closed(project):
    open_applications = Application.objects.filter(project=project).exclude(student_status='ACC')
    for app in open_applications:
        app.professor_status = 'DECL' if app.professor_status != 'OFFR' else 'RETR'
        app.save()
        application_declined(app)

def application_declined(application):
    if application.priority:
        lower_pri_apps = Application.objects.filter(student=application.student, priority__gt=application.priority)
        for lower_pri in lower_pri_apps:
            lower_pri.priority -= 1
            lower_pri.save()
        application.priority = None
        application.save()