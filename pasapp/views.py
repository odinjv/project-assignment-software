from django.contrib.auth import authenticate, login, logout, get_user_model
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.forms import AuthenticationForm
from django.http.response import HttpResponseBadRequest, HttpResponseForbidden, HttpResponseServerError
from django.shortcuts import get_object_or_404, redirect, render
from django.http import HttpResponse, JsonResponse
from django.db.models import F, Q

from pasapp.forms import ApplicationForm, NewProjectForm, NewUserForm,CommentForm
from pasapp.models import Application, Comment, Project, ProjectTag, UserTag, Tag, TagCategory
from pasapp.utils import application_declined, contextWithHeader, get_next, student_accepted, project_closed
from pasapp.templatetags.custom_filters import is_professor, is_student, professor_status, student_status
from pasapp.queries import *


revoked_query = Q(student_status=Application.StudentStatus.RETRACTED) | Q(student_status=Application.StudentStatus.DECLINED)
declined_query = Q(professor_status=Application.ProfessorStatus.DECLINED)
app_closed_query = revoked_query | declined_query

def index(request):
    '''
    Lists all active projects. Also handles filtering those projects via POST requests.
    URL: /
    '''
    included_professor_ids = []
    category_filters = {} # category: [IDs to include, IDs to exclude]
    selected_tags = {} # id: 'I' or 'E'

    if request.method == "POST":
        post_request = request.POST
        # Sort the IDs sent by the filter post request into professor IDs and tag IDs
        for key in post_request.keys():
            if key.startswith("prof"):
                included_professor_ids.append(int(key.replace("prof", "", 1)))
            elif key.startswith("tag"):
                if post_request[key] == '': continue
                id = int(key.replace("tag", "", 1))
                tag = Tag.objects.get(pk=id)
                cat = tag.category
                if cat not in category_filters:
                    category_filters[cat] = ([], [])
                cat_incl, cat_excl = category_filters[cat]
                if post_request[key] == 'I':
                    cat_incl.append(id)
                    selected_tags[id] = 'I'
                elif post_request[key] == 'E':
                    cat_excl.append(id)
                    selected_tags[id] = 'E'
    else:
        user = request.user
        
        if (is_student(user)):
            exclusion_categories = [TagCategory.objects.get(category="Prerequisite Class")] # TODO: Generalize using a field in TagCategory model
            tags = [pair.tag for pair in UserTag.objects.filter(user=user)]
            student_prerequisites = []
            for tag in tags:
                cat = tag.category
                if cat not in category_filters:
                    category_filters[cat] = ([], [])
                cat_incl, cat_excl = category_filters[cat]
                if cat in exclusion_categories:
                    student_prerequisites.append(tag.id)
                else:
                    cat_incl.append(tag.id)
                    selected_tags[tag.id] = 'I'
            missing_prerequisites = Tag.objects.filter(category__in=exclusion_categories).exclude(id__in=student_prerequisites)
            for cat in exclusion_categories:
                category_filters[cat] = ([], [])
            for tag in missing_prerequisites:
                cat_incl, cat_excl = category_filters[tag.category]
                cat_excl.append(tag.id)
                selected_tags[tag.id] = 'E'
    
    projects = get_open_projects()
    project_tags = get_open_tags()

    if included_professor_ids:
        projects = projects.filter(professor_id__in=included_professor_ids)
        project_tags = project_tags.filter(project__in=projects)

    if category_filters:
        for cat in category_filters:
            cat_incl, cat_excl = category_filters[cat]
            if cat_incl:
                cat_project_tags = project_tags.filter(tag_id__in=cat_incl)
                cat_project_ids = [pair.project.id for pair in cat_project_tags]
                projects = projects.filter(id__in=cat_project_ids)
                project_tags = project_tags.filter(project_id__in=cat_project_ids)
            if cat_excl:
                cat_project_tags = project_tags.filter(tag_id__in=cat_excl)
                cat_project_ids = [pair.project.id for pair in cat_project_tags]
                projects = projects.exclude(id__in=cat_project_ids)
                project_tags = project_tags.exclude(project_id__in=cat_project_ids)
    
    application_info = {}
    for project in projects:
        open_apps = Application.objects.filter(project=project).exclude(app_closed_query)
        offered = open_apps.filter(professor_status='OFFR')
        application_info[project.id] = (offered.count(), open_apps.count())

    tag_filters = Tag.objects.all()
    # Only show professors who have published a project
    professor_filters = {
        project.professor for project in get_open_projects()}

    context = {'projects': projects, 'projectTags': project_tags, 'applicationInfo': application_info, 
                'allTags': tag_filters, 'professors': professor_filters,
               'selectedTags': selected_tags, 'selectedProfessors': included_professor_ids}
    return render(request, 'pasapp/pages/projects.html', contextWithHeader(context, request))


def project_details(request, project_id):
    '''
    Shows more details about the given project, including number of applicants
    URL: /project/<id>
    '''
    if is_professor(request.user):
        return professor_project_details(request, project_id)
    else:
        return student_project_details(request, project_id)

def student_project_details(request, project_id):
    project = get_object_or_404(Project, pk=project_id)
    apps = Application.objects.filter(project=project_id)
    num_applicants = apps.count()
    num_offered = apps.filter(professor_status='OFFR').exclude(app_closed_query).count()
    student_application = Application.objects.filter(project=project_id, student=request.user).first() if is_student(request.user) else None
    project_tags = ProjectTag.objects.filter(project=project)
    project_tags = [pair.tag for pair in project_tags]
    comments = Comment.objects.filter(application=student_application)
    context = {'project': project, 'numApplicants': num_applicants, 'numOffered': num_offered, 'projectTags': project_tags, 'student_application': student_application, 'comments': comments}
    return render(request, 'pasapp/pages/student_project_details.html', contextWithHeader(context, request))

def professor_project_details(request, project_id):
    project = get_object_or_404(Project, pk=project_id)
    applications = Application.objects.filter(project=project_id).order_by('-date_created')
    comments = Comment.objects.filter(application__in=applications)
    project_tags = ProjectTag.objects.filter(project=project)
    project_tags = [pair.tag for pair in project_tags]
    context = {'project': project, 'applications': applications, 'comments': comments, 'projectTags': project_tags}
    return render(request, 'pasapp/pages/professor_project_details.html', contextWithHeader(context, request))

@login_required
@user_passes_test(is_professor)
def my_projects(request):
    professor = request.user
    all_projects = Project.objects.filter(professor=professor)

    drafts = all_projects.filter(status='Draft')
    all_projects = all_projects.exclude(status='Draft')

    open_projects = get_open_projects(all_projects)
    other = all_projects.exclude(id__in=[project.id for project in open_projects])

    all_applications = Application.objects.filter(project__in=open_projects) # ignores applications for closed and draft projects
    project_applications = {project.id: 0 for project in open_projects}
    applied_project_ids = all_applications.values_list('project__id', flat=True)
    for id in applied_project_ids:
        project_applications[id] += 1

    applied = open_projects.filter(pk__in=set(applied_project_ids)) # TODO: sort by recent application
    open_projects = open_projects.exclude(pk__in=set(applied_project_ids))

    context = {'drafts': drafts, 'applied': applied, 'open': open_projects, 'other': other, 'projectApplications': project_applications}
    return render(request, 'pasapp/pages/my_projects.html', contextWithHeader(context, request))

@login_required
@user_passes_test(is_student)
def apply(request, project_id):
    '''
    Form page that allows a student to apply for a project
    URL: /project/<id>/apply
    '''
    project = get_object_or_404(Project, pk=project_id)
    if request.method == 'POST':
        form = ApplicationForm(request.POST)
        if form.is_valid():
            student = request.user
            message = form.cleaned_data.get('message')
            preempt = form.cleaned_data.get('preempt')
            student_status = Application.StudentStatus.PREEMPTED if preempt else Application.StudentStatus.APPLIED
            application = Application(
                project=project, student=student, message=message, student_status=student_status)
            application.save()

            User = get_user_model()
            group_users = form.cleaned_data.get('additional_students').split(' ')
            for student in group_users:
                if student == '':
                    continue
                student_object = User.objects.get(username=student)
                application.group_members.add(student_object)
            return redirect(f"/project/{project_id}/")
    form = ApplicationForm()
    context = {'project': project, 'form': form}
    return render(request, 'pasapp/pages/create_application.html', contextWithHeader(context, request))


@login_required
def project_applications(request, project_id): # semi-deprecated
    '''
    Shows all applications to a project (if you are the professor who owns that project)
    URL: /project/<id>/applications
    '''
    project = get_object_or_404(Project, pk=project_id)
    if not request.user.id == project.professor.id:
        return HttpResponseForbidden()
    applications = Application.objects.filter(
        project=project_id).order_by('-date_created')
    comments = Comment.objects.filter(application__in=applications)
    context = {'project': project, 'applications': applications, 'comments': comments}
    return render(request, 'pasapp/pages/project_applications.html', contextWithHeader(context, request))


@login_required
@user_passes_test(is_professor)
def create_project(request):
    '''
    Form page allowing professors to create a new project
    URL: /project/create
    '''
    if request.method == 'POST':
        form = NewProjectForm(request.POST)
        if form.is_valid():
            title = form.cleaned_data.get('title')
            description = form.cleaned_data.get('description')
            capacity = form.cleaned_data.get('capacity')
            professor = request.user
            status = form.cleaned_data.get('status')
            if status in DEFAULT_STATUSES:
                hidden = False if status == OPEN_STATUS else True
            else:
                hidden = form.cleaned_data.get('hidden')
            project = Project(title=title, description=description, capacity=capacity,
                              status=status, hidden=hidden, professor=professor)
            project.save()

            tags = form.cleaned_data.get('tags')
            tag_list = tags.split()
            for tag_id_str in tag_list:
                tag_id = int(tag_id_str)
                tag = Tag.objects.get(pk=tag_id)
                if tag is None:
                    raise ValueError("Unknown tag found")
                project_tag = ProjectTag(project=project, tag=tag)
                project_tag.save()

            return redirect(get_next(request))
    form = NewProjectForm()
    tags = Tag.objects.all()
    next = request.GET.get('next', '')
    context = {'form': form, 'tags': tags, 'next': next}
    return render(request, 'pasapp/pages/create_project.html', contextWithHeader(context, request))


@login_required
@user_passes_test(is_professor)
def edit_project(request, project_id):
    '''
    Form page that allows a professor to edit an existing project.
    URL: /project/<id>/edit
    '''
    project = get_object_or_404(Project, pk=project_id)
    if not request.user.id == project.professor.id:
        return HttpResponseForbidden()
    
    project_tags = ProjectTag.objects.filter(project=project.id)

    if request.method == 'POST':
        form = NewProjectForm(request.POST)
        if form.is_valid():
            title = form.cleaned_data.get('title')
            description = form.cleaned_data.get('description')
            capacity = form.cleaned_data.get('capacity')
            status = form.cleaned_data.get('status')
            if status in DEFAULT_STATUSES:
                hidden = False if status == OPEN_STATUS else True
            else:
                hidden = form.cleaned_data.get('hidden')
            project.title = title
            project.description = description
            project.capacity = capacity
            project.status = status
            project.hidden = hidden
            project.save()

            tags = form.cleaned_data.get('tags')
            tag_list = tags.split()
            project_tag_id_list = [project_tag.tag.id for project_tag in project_tags]
            for tag_id_str in tag_list:
                tag_id = int(tag_id_str)
                if tag_id in project_tag_id_list:
                    continue
                tag = Tag.objects.get(pk=tag_id)
                if tag is None:
                    raise ValueError("Unknown tag found")
                project_tag = ProjectTag(project=project, tag=tag)
                project_tag.save()
            project_tags.exclude(tag__in=[int(tag) for tag in tag_list]).delete()

            return redirect(get_next(request, f'/project/{project_id}/'))

    else:
        form_tags = ' '.join(str(project_tag.tag.id) for project_tag in project_tags)
        form = NewProjectForm(initial={
                              'title': project.title, 'description': project.description, 'capacity': project.capacity, 
                              'status': project.status, 'hidden': project.hidden, 'tags':form_tags})
    tags = Tag.objects.all()
    project_tags = [pair.tag for pair in project_tags]
    next = request.GET.get('next', '')
    context = {'form': form, 'project': project, 'tags': tags, 'projectTags': project_tags, 'next': next}
    return render(request, 'pasapp/pages/edit_project.html', contextWithHeader(context, request))

@login_required
@user_passes_test(is_professor)
def copy_project(request, project_id):
    '''
    GET route that copies an existing project into a new one. Saving is handled by create_project
    URL: /project/update_status/
    '''
    if request.method == 'GET':
        project = get_object_or_404(Project, pk=project_id)
        if not request.user.id == project.professor.id:
            return HttpResponseForbidden()
        project_tags = ProjectTag.objects.filter(project=project.id)
        form_tags = ' '.join(str(project_tag.tag.id) for project_tag in project_tags)
        project_dict = {
            'title': project.title,
            'description': project.description,
            'capacity': project.capacity,
            'professor': request.user,
            'group_size': project.group_size,
            'tags': form_tags
        }
        form = NewProjectForm(initial=project_dict)
        tags = Tag.objects.all()
        project_tags = [pair.tag for pair in project_tags]
        context = {'form': form, 'project': project, 'tags': tags, 'projectTags': project_tags}
        return render(request, 'pasapp/pages/copy_project.html', contextWithHeader(context, request))
    else:
        return HttpResponseBadRequest("This endpoint only supports GET requests.")

@login_required
@user_passes_test(is_professor)
def set_project_status(request):
    '''
    POST route that edits the status of an existing project.
    URL: /project/update_status/
    '''
    if request.method == 'POST':
        project_id = request.POST['project']
        project = get_object_or_404(Project, pk=project_id)
        if not request.user.id == project.professor.id:
            return HttpResponseForbidden()
        status = request.POST['action']
        project.status = status
        if status in DEFAULT_STATUSES:
            project.hidden = False if status == OPEN_STATUS else True
        project.save()
        if status == 'Closed' or project.hidden:
            project_closed(project)
        return redirect(get_next(request))
    else:
        return HttpResponseBadRequest("This endpoint only supports POST requests.")

@login_required
@user_passes_test(is_professor)
def delete_project(request):
    '''
    POST route that deletes an existing project.
    URL: /project/delete/
    '''
    if request.method == 'POST':
        project_id = request.POST['project']
        project = get_object_or_404(Project, pk=project_id)
        if not request.user.id == project.professor.id:
            return HttpResponseForbidden()
        project_closed(project)
        project.delete()
        return redirect(get_next(request))
    else:
        return HttpResponseBadRequest("This endpoint only supports POST requests.")

@login_required
def my_applications(request):
    '''
    Displays all of the user's applications, rendered differently for students and professors (see below)
    URL: /applications
    '''
    if is_student(request.user):
        return student_applications_view(request)
    elif is_professor(request.user):
        return professor_applications_view(request)
    else:
        return HttpResponse("Only students and professors may view this page.")


def student_applications_view(request):
    '''
    Displays all of the student's project applications and allows them to set rankings (see update_priorities below)
    '''
    my_applications = Application.objects.filter(student=request.user)
    group_applications = request.user.member_projects.all()
    all_applications = (my_applications | group_applications).distinct()

    closed_applications = all_applications.filter(app_closed_query)
    all_applications = all_applications.exclude(app_closed_query)
    selectable_applications = my_applications.exclude(app_closed_query)

    offered_applications = all_applications.filter(professor_status=Application.ProfessorStatus.OFFERED)
    open_applications = all_applications.filter(professor_status=Application.ProfessorStatus.NONE)

    comments = Comment.objects.filter(application__in=my_applications)
    context = {'applications': selectable_applications, 'offeredApplications': offered_applications, 
        'openApplications': open_applications, 'closedApplications': closed_applications, 
        'comments': comments}
    return render(request, 'pasapp/pages/student_applications.html', contextWithHeader(context, request))


def professor_applications_view(request):
    '''
    Displays all applications to the professor's open projects, grouped by project
    '''
    projects = get_open_projects(Project.objects.filter(professor=request.user))
    sections = {project: Application.objects.filter(
        project=project).order_by('-date_created') for project in projects} # TODO: should be last_updated
    comments = Comment.objects.filter(application__in=Application.objects.filter(project__in=projects))
    context = {'sections': sections, 'comments': comments}
    return render(request, 'pasapp/pages/professor_applications.html', contextWithHeader(context, request))

@login_required
@user_passes_test(is_student)
def update_priorities(request):
    '''
    Endpoint for updating the priority values of a student's applications. POST only.
    URL: /applications/update_priorities
    '''
    if request.method == 'POST':
        post_request = request.POST
        errors = []
        for field in post_request:
            if field.startswith("priority"):
                application_id = post_request[field]
                priority = int(field.replace("priority", "", 1))
                application = None

                if application_id != '':
                    application = Application.objects.get(pk=application_id)
                    if application.student != request.user:
                        errors.append(f"Application with priority {priority} did not belong to you.")
                        continue

                duplicate_priority_applications = Application.objects.filter(
                    student=request.user, priority=priority)
                for application in duplicate_priority_applications:
                    application.priority = None
                    application.save()

                if application_id == '':
                    continue

                if application is not None:
                    application.priority = priority
                    application.save()
        if errors:
            return HttpResponseForbidden("\n".join(errors))
        return redirect("/applications/")

    else:
        return HttpResponseBadRequest("This endpoint only supports POST requests.")

@login_required
@user_passes_test(is_student)
def update_student_status(request):
    '''
    Endpoint for changing the student status of a student's application. POST only.
    URL: /applications/update_student_status
    '''
    if request.method == 'POST':
        post_request = request.POST
        application_id = post_request['application']
        state = post_request['state']
        application = Application.objects.get(pk=application_id)

        if state == 'LEAVE':
            application.group_members.remove(request.user)
            return redirect(get_next(request,'/applications/'))

        if request.user != application.student:
            return HttpResponseBadRequest("This application is not yours.")

        if state in ('PRE', 'APPL'):
            if application.student_status not in (Application.StudentStatus.PREEMPTED, Application.StudentStatus.APPLIED):
                return HttpResponseBadRequest("This application could not be edited.")
            if state == 'PRE':
                application.student_status = Application.StudentStatus.PREEMPTED
            else:
                application.student_status = Application.StudentStatus.APPLIED
        elif state in ('ACC', 'DECL'):
            if application.professor_status != 'OFFR':
                return HttpResponseBadRequest("This application has no offer to respond to.")
            if state == 'ACC': # TODO: ensure students cant accept multiple, call other stuff
                application.student_status = Application.StudentStatus.ACCEPTED
            else: # TODO: ensure application is removed from priority, call other stuff
                application.student_status = Application.StudentStatus.DECLINED
        elif state == 'RETR': # TODO: ensure application is removed from priority, call other stuff
            application.student_status = Application.StudentStatus.RETRACTED
        application.save()

        if state == 'ACC':
            student_accepted(application)
        if state == 'RETR' or state == 'DECL':
            application_declined(application)
        return redirect(get_next(request,'/applications/'))

    else:
        return HttpResponseBadRequest("This endpoint only supports POST requests.")

@login_required
@user_passes_test(is_professor)
def update_professor_status(request):
    '''
    Endpoint for changing the professor status of a student's application. POST only.
    URL: /applications/update_professor_status
    '''
    if request.method == 'POST':
        post_request = request.POST
        application_id = post_request['application']
        action = post_request['action'] if 'action' in post_request else post_request['status']
        application = Application.objects.get(pk=application_id)
        if request.user != application.project.professor:
            return HttpResponseBadRequest("This application is not yours.")
        if application.student_status not in (Application.StudentStatus.PREEMPTED, Application.StudentStatus.APPLIED):
            return HttpResponseBadRequest("This application is not open for response.")
        if application.professor_status == Application.ProfessorStatus.OFFERED:
            return HttpResponseBadRequest("This offer has already been extended.")

        if action == 'offer':
            application.professor_status = Application.ProfessorStatus.OFFERED
            if application.student_status == 'PRE':
                application.student_status = 'ACC'
        elif action == 'decline':
            application.professor_status = Application.ProfessorStatus.DECLINED
        else:
            return HttpResponseBadRequest("Unknown action.") 
        application.save()
        
        if application.student_status == 'ACC':
            student_accepted(application)
        elif action == 'decline':
            application_declined(application)
        return redirect(get_next(request,'/applications/'))

    else:
        return HttpResponseBadRequest("This endpoint only supports POST requests.")

@login_required
def add_comment(request):
    '''
    Endpoint for adding a comment to an application. POST only.
    URL: /applications/add_comment
    '''
    if request.method == 'POST':
        form = CommentForm(data=request.POST)
        if form.is_valid():
            message = form.cleaned_data.get('message')
            application = form.cleaned_data.get('application')
            sender = request.user
            comment = Comment(application_id=application, message=message, sender=sender)
            comment.save()
            return redirect(get_next(request))
    else:
        return HttpResponseBadRequest("This endpoint only supports POST requests.")

def profile_page(request, username):
    '''
    Displays personal tags of a user.
    URL: /user/<str:username>/
    '''
    User = get_user_model()
    profile_user = User.objects.get(username=username)
    user_tags = UserTag.objects.filter(user=profile_user)
    if request.method == 'POST':
        if profile_user != request.user:
            return HttpResponseForbidden("You may not edit the tags of another user.")
        tags = request.POST['tags']
        tag_list = tags.split()
        user_tag_id_list = [user_tag.tag.id for user_tag in user_tags]
        for tag_id_str in tag_list:
            tag_id = int(tag_id_str)
            if tag_id in user_tag_id_list:
                continue
            tag = Tag.objects.get(pk=tag_id)
            if tag is None:
                raise ValueError("Unknown tag found")
            user_tag = UserTag(user=request.user, tag=tag)
            user_tag.save()
        user_tags.exclude(tag__in=[int(tag) for tag in tag_list]).delete()
        return redirect(f'/user/{request.user}/')
    else:
        if is_student(request.user) and is_student(profile_user) and profile_user.username != request.user.username:
            return HttpResponseForbidden("You may not view the profile of another student.")
        tags = Tag.objects.all() # TODO: limit by what tags are valid
        user_tags = [pair.tag for pair in user_tags]
        context = {'profileUser': profile_user, 'userTags': user_tags, 'tags': tags}
        return render(request, 'pasapp/pages/profile.html', contextWithHeader(context, request))

def get_student(request, username):
    if (username == request.user.username):
        return JsonResponse({'error': 'You are already a member of this project.'})
    User = get_user_model()
    student = get_object_or_404(User, username=username)
    if (is_student(student)):
        return JsonResponse({'name': f'{student.first_name} {student.last_name}'})
    return JsonResponse({'error': 'User is not a student.'})

def login_view(request):
    '''
    Form page for logging a user in. Mostly used as a redirect if the user attempts to access
    a page that requires login.
    URL: /login
    '''
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('index')
        print(form.non_field_errors())
    else:
        form = AuthenticationForm()
    return render(request, 'pasapp/pages/login.html', {'form': form})


def logout_view(request):
    '''
    Endpoint for logout requests. Logs the current user out and returns to the index page.
    URL: /logout
    '''
    logout(request)
    return redirect('index')

def register(request):
    '''
    Form page for registering a new user.
    URL: /register
    '''
    if request.method == 'POST':
        form = NewUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            return redirect("index")
    else:
        form = NewUserForm()
    context = {'form': form}
    return render(request, 'pasapp/pages/register.html', contextWithHeader(context, request))
