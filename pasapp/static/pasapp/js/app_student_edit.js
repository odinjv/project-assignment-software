function checkForConflicts(select) {
    const allSelects = document.getElementsByClassName("prioritySelect");
    for (selectEl of allSelects) {
        if (selectEl.name !== select.name && selectEl.value === select.value) {
            selectEl.value = "";
        }
    }
}

function updateAppStatus(checkbox) {
    let state;
    if (checkbox.checked) {
        state = 'PRE'
    } else {
        state = 'APPL'
    }
    submitAction(checkbox.id, state)
}

function acceptOffer(id) {
    submitAction(id, 'ACC');
}

function declineOffer(id) {
    submitAction(id, 'DECL');
}

function retractApplication(id) {
    submitAction(id, 'RETR');
}

function leaveGroup(id) {
    submitAction(id, 'LEAVE');
}

function submitAction(id, action) {
    let form = document.getElementById("status_edit_form");
    let idField = document.getElementById("status_id");
    let stateField = document.getElementById("status_state");
    idField.value = id;
    stateField.value = action;
    form.submit();
}