function filterCheckbox(box) {
    const parent = box.parentElement
    const field = parent.querySelector('input[type=hidden]')

    const boxClass = box.classList.contains('cross') ? 'cross' : 'check';
    const siblingClass = (boxClass == 'cross') ? 'check' : 'cross';
    const sibling = parent.querySelector('.' + siblingClass);

    box.classList.toggle("checked");

    let val = ''
    if (isChecked(box)) {
        val = (boxClass == 'check') ? 'I' : 'E';
        uncheck(sibling);
    }
    field.value = val;
}

function filterLabel(label) {
    const parent = label.parentElement
    const checkBox = parent.querySelector('.check');
    const crossBox = parent.querySelector('.cross');
    
    const field = parent.querySelector('input[type=hidden]')
    let val = '';

    if (!isChecked(checkBox) && !isChecked(crossBox)) {
        check(checkBox);
        val = 'I';
    } else if (isChecked(checkBox)) {
        uncheck(checkBox);
        check(crossBox);
        val = 'E';
    } else if (isChecked(crossBox)) {
        uncheck(crossBox);
    }
    field.value = val;
}

function isChecked(div) {
    return div.classList.contains('checked');
}

function check(div) {
    div.classList.add('checked');
}

function uncheck(div) {
    div.classList.remove('checked');
}