let initialStatus = "";

function setInitialStatus() {
    const value = initialStatus;
    if (value == "") {
        return
    }
    const dropdown = document.getElementById("status-dropdown");
    for (option of dropdown.options) {
        if (option.value == value) {
            dropdown.value = value;
            updateStatus(value);
            return;
        }
    }
    const customCheckbox = document.getElementById("custom-status-checkbox");
    const field = document.getElementById("custom-status-textfield");
    const hiddenCheckboxValue = document.getElementById("id_hidden").checked;
    const hiddenCheckbox = document.getElementById("custom-status-visibility-checkbox");
    customCheckbox.checked = true;
    field.value = value;
    hiddenCheckbox.checked = hiddenCheckboxValue;    
}

function setStatus(list) {
    const checkbox = document.getElementById("custom-status-checkbox");
    if (checkbox.checked) {
        checkbox.checked = false;
    }
    const value = list.value;
    updateStatus(value);
}

function toggleCustomStatus(checkbox) {
    const isCustom = checkbox.checked;
    if (isCustom) {
        let value = document.getElementById("custom-status-textfield").value;
        updateStatus(value);
    } else {
        let value = document.getElementById("status-dropdown").value;
        updateStatus(value);
    }
}

function toggleCustomStatusVisibility(checkbox) {
    const isHidden = checkbox.checked;
    const statusForm = document.getElementById("id_hidden");
    statusForm.checked = isHidden;
}

function setCustomStatus(field) {
    const checkbox = document.getElementById("custom-status-checkbox");
    if (!checkbox.checked) {
        checkbox.checked = true;
    }
    const value = field.value;
    updateStatus(value);
}

function updateStatus(value) {
    const statusForm = document.getElementById("id_status");
    statusForm.value = value;
}