function addNextRoutes() {
    for (form of document.forms) {
        if (typeof form.action === 'string') {
            form.action = form.action.replace("#NEXT", "?next=" + window.location.pathname);
        }
    }
};
