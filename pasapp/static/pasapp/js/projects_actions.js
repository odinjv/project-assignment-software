function deleteProject(id) {
    let form = document.getElementById('delete_project_form');
    document.getElementById('delete_id').value = id;
    form.submit()
}

function publishProject(id) {
    updateStatus(id, 'Open')
}

function closeProject(id) {
    updateStatus(id, 'Closed')
}

function updateStatus(id, status) {
    let form = document.getElementById('update_status_form');
    document.getElementById('status_id').value = id;
    document.getElementById('status_action').value = status;
    form.submit()
}