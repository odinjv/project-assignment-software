const userSet = new Set();

function memberKeyDown(event) {
    if (event.key === "Enter" || event.keyCode === 13) {
        event.preventDefault();
        addUser();
    }
}

function addUser() {
    const input = document.getElementById("user-entry");
    const userBox = document.getElementById("user-box");
    const errorBox = document.getElementById("user-error-notifier")

    const username = input.value;

    if (username == '') {
        return;
    }

    if (userSet.has(username)) {
        input.value = "";
        errorBox.classList.remove('hidden');
        errorBox.innerHTML = `⚠ ${username} has already been added.`;
        return;
    }

    fetch(`/api/get_student/${username}`)
        .then(response => response.json())
        .then(data => {
            if (data.error) {
                errorBox.classList.remove('hidden');
                errorBox.innerHTML = "⚠ " + data.error;
            } else {
                userBox.innerHTML += `<div class="user-bubble" onclick="removeUser(this,'${username}')">${data.name}</div>`;
                
                input.value = "";
                userSet.add(username);
                errorBox.classList.add('hidden');
                updateForm();
            }
        })
        .catch(e => {
            errorBox.classList.remove('hidden');
            errorBox.innerHTML = `⚠ student "${username}" was not found.`;
        });
}

function removeUser(item, username) {
    userSet.delete(username);
    item.remove();
    updateForm()
}

function updateForm() {
    const userForm = document.getElementById("id_additional_students");
    userForm.value = [...userSet].join(' ');
}