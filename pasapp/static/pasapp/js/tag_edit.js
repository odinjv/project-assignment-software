const tagSet = new Set();

function addTag(list) {
    const tagId = parseInt(list.value);
    const tagName = list.options[list.selectedIndex].text;
    const tagCategory = list.options[list.selectedIndex].attributes['meta'].value;
    const tagCategorySlug = tagCategory.toLowerCase().replaceAll(' ', '-')

    if (tagSet.has(tagId)) {
        return;
    }
    tagSet.add(tagId);

    const tagBox = document.getElementById("tagBox");
    if (tagBox.classList.contains("categorized")) {
        for (category of tagBox.children) {
            if (category.attributes['meta'].value == tagCategory) {
                category.innerHTML += `<li class="tag tag-bubble ${tagCategorySlug}" onclick="removeTag(this, ${tagId})">${tagName}</li>`;
                updateForm();
                return;
            }
        }
        // If the category was not found
        let newInner = `<ul class="tagCategory ${tagCategorySlug}" meta="${tagCategory}">`
        newInner += `<h3>${tagCategory}</h3>`
        newInner += `<li class="tag tag-bubble ${tagCategorySlug}" onclick="removeTag(this, ${tagId})">${tagName}</li>`
        newInner += `</ul>`
        tagBox.innerHTML += newInner
    } else {
        console.log("fuck");
        tagBox.innerHTML += `<li class="tag tag-bubble ${tagCategorySlug}" onclick="removeTag(this, ${tagId})">${tagName}</li>`;
    }
    // <li class="tag {{ tag.category|lower|slugify }}" title="{{ tag.category}}">{{tag.name}}</li>
    //tagBox.innerHTML += `<li class="tag tag-bubble ${tagCategorySlug}" onclick="removeTag(this, ${tagId})">${tagName}</li>`;

    updateForm();
}

function removeTag(item, tagId) {
    tagSet.delete(tagId);
    parent = item.parentElement
    if (parent.classList.contains("tagCategory") && parent.children.length == 2) {
        item.remove();
        parent.remove();
    } else {
        item.remove();
    }

    updateForm();
}

function updateForm() {
    const tagForm = document.getElementById("id_tags");
    tagForm.value = [...tagSet].join(' ');
}