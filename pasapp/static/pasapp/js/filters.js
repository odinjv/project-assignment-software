function collapse(div) {
    const collapsibleDiv = div.parentNode.querySelector(".collapsible");
    collapsibleDiv.classList.toggle("collapsed");
}

function clearFilters() {
    const columns = document.getElementsByClassName('filter-column')
    const tagFilters = columns[0]
    const profFilters = columns[1]
    for (box of tagFilters.querySelectorAll('.custom-checkbox.checked')) {
        filterCheckbox(box);
    }
    for (box of profFilters.querySelectorAll('input[type=checkbox]:checked')) {
        box.checked = false;
    }
}