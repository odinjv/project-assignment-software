function offerApplication(id) {
    submitAction(id, 'offer')
}

function declineApplication(id) {
    submitAction(id, 'decline');
}

function submitAction(id, action) {
    let form = document.getElementById("status_edit_form");
    let idField = document.getElementById("status_id");
    let actionField = document.getElementById("status_action");
    idField.value = id;
    actionField.value = action;
    form.submit();
}