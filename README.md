# NTNU Project Assignment Service Prototype

A prototype implementation of a service for assigning projects such as master's
and bachelor's assignments, as well as potentially other types of projects.

## Project setup

This project is written in Python using Django. In order to run it locally we
recommend setting up a virtual environment, using e.g. Python's `venv` module:

* Create the virtual environment: `python -m venv venv`
* Activate the virtual environment
  * On Windows
    * Using cmd.exe: `.\venv\Scripts\activate.bat`
    * Using PowerShell: `.\venv\Scripts\Activate.ps1`
  * On POSIX (Linux, macOS, BSD etc.): `source venv/bin/activate`
* Install the required packages: `pip install -r requirements.txt`
* Apply database migrations: `python manage.py migrate`
* Optionally populate the database: `python manage.py populatedefaults` `python manage.py populatesamples`
  * or `python manage.py loadcsvdata test_data/[data folder]`
* Run the application: `python manage.py runserver`
  * This will make the application available on port 8000; visit it by going
    to https://localhost:8000/ in your browser.
* Deactivate the virtual environment when you are done: `deactivate`
